-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: bgb
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `username` varchar(110) DEFAULT NULL,
  `password` varchar(110) DEFAULT NULL,
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES ('admin','bgb',1),('shahad','wewe',2);
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `capture`
--

DROP TABLE IF EXISTS `capture`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `capture` (
  `id` int(11) NOT NULL,
  `soldier_id` int(11) DEFAULT NULL,
  `pillar_id` int(11) DEFAULT NULL,
  `time_captured` varchar(110) DEFAULT NULL,
  `situation` varchar(110) DEFAULT NULL,
  `image_url` time DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `capture`
--

LOCK TABLES `capture` WRITE;
/*!40000 ALTER TABLE `capture` DISABLE KEYS */;
INSERT INTO `capture` VALUES (1,1,1,'2017-01-01-12-48','GOOD',NULL),(2,1,1,'2017-04-01-12-48','POOR',NULL),(3,2,3,'2017-06-01-02-48','MODERATE',NULL),(4,1,2,'2017-11-01-10-48','GOOD',NULL),(5,3,1,'2017-01-21-02-48','GOOD',NULL),(6,1,3,'2017-01-22-12-48','MODERATE',NULL),(7,4,3,'2017-01-22-13-48','POOR',NULL),(8,1,1,'2017-02-02-12-48','MODERATE',NULL),(9,2,3,'2017-02-02-20-48','GOOD',NULL),(10,1,6,'2017-03-02-11-48','MODERATE',NULL);
/*!40000 ALTER TABLE `capture` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pillar`
--

DROP TABLE IF EXISTS `pillar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pillar` (
  `id` int(11) NOT NULL,
  `name` varchar(110) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `longitude` varchar(110) DEFAULT NULL,
  `latitude` varchar(110) DEFAULT NULL,
  `situation` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pillar`
--

LOCK TABLES `pillar` WRITE;
/*!40000 ALTER TABLE `pillar` DISABLE KEYS */;
INSERT INTO `pillar` VALUES (1,'South Pillar-1',1,'91.088881','24.0100101','null'),(2,'South Pillar-2',2,'0.0','0.0','Average'),(3,'North Pillar',3,'12434.441234','23481.3424','Moderate'),(4,'West Pillar-1',4,'1875234.2244','23234541.31244','Flooded'),(5,'West Pillar-1',5,'121434.4441','237941.3134','Broken'),(6,'East Pillar',6,'1253441.42434','23471.34134','Missing'),(8,'as',2,'11','1111',NULL),(10,'null',NULL,'null','null','null'),(11,'null',NULL,'null','null','null'),(13,'ids-01',0,'24.914038','91.952716','good'),(14,'null',NULL,'null','null','null'),(15,'null',NULL,'null','null','null'),(16,'null',NULL,'null','null','null'),(17,'ids-01',0,'24.913975','91.953658','good'),(18,'null',NULL,'null','null','null'),(19,'null',NULL,'null','null','null'),(20,'null',NULL,'null','null','null'),(21,'null',NULL,'null','null','null'),(22,'null',NULL,'null','null','null'),(23,'null',NULL,'null','null','null'),(24,'null',NULL,'null','null','null'),(25,'null',NULL,'null','null','null'),(26,'null',NULL,'null','null','null'),(27,'null',NULL,'null','null','null'),(28,'null',NULL,'null','null','null'),(29,'null',NULL,'null','null','null'),(30,'null',NULL,'null','null','null'),(31,'null',NULL,'null','null','null'),(32,'null',NULL,'91.088881','24.0100101','null'),(33,'null',NULL,'0.0','0.0','Average'),(34,'null',NULL,'0.0','0.0','Good'),(35,'null',NULL,'0.0','0.0','Good');
/*!40000 ALTER TABLE `pillar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `soldier`
--

DROP TABLE IF EXISTS `soldier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `soldier` (
  `id` int(11) NOT NULL,
  `name` varchar(110) DEFAULT NULL,
  `designation` varchar(110) DEFAULT NULL,
  `username` varchar(110) DEFAULT NULL,
  `password` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `soldier`
--

LOCK TABLES `soldier` WRITE;
/*!40000 ALTER TABLE `soldier` DISABLE KEYS */;
INSERT INTO `soldier` VALUES (1,'Ali','Soldier(BGB-2)','ali','qrwe'),(2,'Mohammad','Soldier(BGB-11)','md','reqrd'),(3,'Sohel','Soldier(BGB-3)','sohel','rqwerq'),(4,'Bibhash','Soldier(BGB-1)','bibhash','afsdfa'),(5,'Mirza','Soldier(BGB-2)','mirza','dafa'),(6,'Shahad','Soldier(BGB-1)','shahad','asdfasd'),(7,'Al- Amin','Captain','alamin','afsd'),(8,'ANN','afsd asdf','ann','das'),(9,'Jubayer','adg','jubayer','erwer'),(10,'asd','adg','a','ter'),(11,'asd','adg','b','wwetr'),(13,'werqw','qqq','c','wertwert'),(14,'werqw','qqq','d','wtert'),(15,'pp','ppp','e','wter'),(16,'Salam','Soldier','salam','tewr'),(17,'www','wer','asdf','wer'),(18,'alamin_sust','qqqq','qqq','H7R1feGI');
/*!40000 ALTER TABLE `soldier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `soldier_location`
--

DROP TABLE IF EXISTS `soldier_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `soldier_location` (
  `id` int(11) DEFAULT NULL,
  `soldier_id` int(11) DEFAULT NULL,
  `longitude` varchar(110) DEFAULT NULL,
  `latitude` varchar(110) DEFAULT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `soldier_location`
--

LOCK TABLES `soldier_location` WRITE;
/*!40000 ALTER TABLE `soldier_location` DISABLE KEYS */;
INSERT INTO `soldier_location` VALUES (0,0,'0','0','2017-02-25 15:50:24'),(1,9,'91.108881','24.919200','2017-02-25 16:31:00'),(2,5,'91.95351692252466','24.91271444795875','2017-02-25 19:16:59'),(3,9,'91.95353545215123','24.912703212646132','2017-02-25 19:02:11'),(4,7,'91.95352814229288','24.912735443882898','2017-02-25 21:22:00'),(5,4,'91.95360765848207','24.9127406901786','2017-02-25 19:16:59'),(6,3,'91.95355428644717','24.91273380539531','2017-02-25 19:26:59'),(7,7,'91.95359038395273','24.912725170798126','2017-02-25 20:21:00'),(8,1,'91.95362212586343','24.912689837689225','2017-02-25 22:06:59'),(9,1,'91.95360277341932','24.91271066192659','2017-02-25 19:02:59'),(10,9,'91.95359548544893','24.91271687379369','2017-02-25 19:05:41'),(11,5,'91.95356816774496','24.912689860898166','2017-02-25 19:06:59'),(12,9,'91.95361963974754','24.912679813850172','2017-02-25 19:06:41'),(13,9,'91.95357330575273','24.912684171849925','2017-02-25 19:07:11'),(14,2,'91.95357026575837','24.912670692187508','2017-02-25 21:31:00'),(15,2,'91.95357026575837','24.912670692187508','2017-02-25 21:33:00'),(16,4,'91.95333707727873','24.912607579424943','2017-02-25 21:21:00'),(17,2,'91.95333707727873','24.912607579424943','2017-02-25 21:21:00'),(18,4,'91.95336995503558','24.91263792241824','2017-02-25 21:31:00'),(19,9,'91.95347269252316','24.912693188012668','2017-02-25 19:12:13'),(20,9,'91.95368089337306','24.91279692393051','2017-02-25 19:12:42'),(21,9,'91.95364495229016','24.91275073162767','2017-02-25 19:13:12'),(22,9,'91.95363582141856','24.912823593407367','2017-02-25 19:13:42'),(23,9,'91.95356007019511','24.91275414202106','2017-02-25 19:14:12'),(24,9,'91.95352061629336','24.912749962984954','2017-02-25 19:14:42'),(25,9,'91.95360456257183','24.912737056217466','2017-02-25 19:29:20'),(26,9,'91.953401437583','24.91286554462779','2017-02-26 21:28:58'),(27,9,'91.9535075313949','24.912794560116705','2017-02-26 21:29:28'),(28,9,'91.95351942010774','24.912780228605094','2017-02-26 21:29:58'),(29,9,'91.95351942010774','24.912780228605094','2017-02-26 21:30:28'),(30,9,'91.95356144875778','24.912727836604585','2017-02-26 21:30:58'),(31,9,'91.95356144875788','24.91272783660444','2017-02-26 21:31:28'),(32,9,'91.95356144875788','24.91272783660444','2017-02-26 21:31:59'),(33,9,'91.95363269029313','24.912620487417705','2017-02-26 21:32:29'),(34,9,'91.95366591848061','24.912533363288464','2017-02-26 21:32:59'),(35,9,'91.95373000479901','24.912426034488874','2017-02-26 21:33:29'),(36,9,'91.95372774831317','24.91241420291324','2017-02-26 21:33:59'),(37,9,'91.95372774831317','24.91241420291324','2017-02-26 21:34:29'),(38,9,'91.95363759259753','24.912707567325366','2017-02-26 21:35:00'),(39,9,'91.95359815984415','24.91275528421229','2017-02-26 21:35:29'),(40,9,'91.95359815984415','24.91275528421229','2017-02-26 21:35:59'),(41,9,'91.95367533251238','24.912755948726222','2017-02-26 21:36:29');
/*!40000 ALTER TABLE `soldier_location` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-02-27  4:35:55
