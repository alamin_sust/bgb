<%-- 
    Document   : pillarHistory
    Created on : Feb 26, 2017, 2:02:16 AM
    Author     : Al-Amin
--%>




<%@page import="java.util.Collections"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.io.File"%>
<%@page import="java.util.List"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="com.bgb.connection.Database"%>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Pillar History</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
    /* Remove the navbar's default rounded borders and increase the bottom margin */ 
    .navbar {
      margin-bottom: 50px;
      border-radius: 0;
    }
    
    /* Remove the jumbotron's default bottom margin */ 
     .jumbotron {
      margin-bottom: 0;
    }
   
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
  </style>
</head>
<body>
<%
    if(session.getAttribute("userId")==null){
                    response.sendRedirect("login.jsp");
                }
    
             Database db = new Database();
                db.connect();
                Statement stmt = db.connection.createStatement();
                
                
                
                if(request.getParameter("logout")!=null) {
                    session.setAttribute("userId", null);
                }
                
                String username = request.getParameter("username");
                String password = request.getParameter("password");
                if(username != null && password != null) {
                    String query = "select id,password from admin where username like '"+username+"'";
                    ResultSet rs = stmt.executeQuery(query);
                    while(rs.next()==true && request.getParameter("password").equals(rs.getString("password")) ) {
                         session.setAttribute("userId", rs.getString("id") );
                         session.setAttribute("username", username );
                    }
                }
    %>
<div class="jumbotron" style="background-image: url(bg2.jpg); background-size: 100%;">
  <div class="container text-center">
    <img src="logo-bgb.jfif" class="img-circle" height="105" width="105" alt="Avatar"/>
    <b><h2 style="color: white">BGB Patrol Monitoring & Border Condition Reporting</h2></b>
  </div>
    <form action="pillar.jsp" method="get" class="navbar-form navbar-right" role="search">
        <div class="form-group input-group">
            <input type="text" name="findpillar" class="form-control" placeholder="Search Pillar" required="">
          <span class="input-group-btn">
            <button class="btn btn-default" type="submit">
              <span class="glyphicon glyphicon-search"></span>
            </button>
          </span>        
        </div>
      </form>
        <form action="soldier.jsp" method="get" class="navbar-form navbar-right" role="search">
        <div class="form-group input-group">
            <input type="text" name="findsoldier" class="form-control" placeholder="Search Soldier" required="">
          <span class="input-group-btn">
            <button class="btn btn-default" type="submit">
              <span class="glyphicon glyphicon-search"></span>
            </button>
          </span>        
        </div>
      </form>
</div>
   
    <nav class="navbar navbar-inverse" style="color: white; background: darkkhaki">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar"  >
      <ul class="nav navbar-nav" >
        <li><a href="home.jsp" style="color: lightyellow"><b>Home</b></a></li>
        <li><a href="soldier.jsp" style="color: lightyellow"><b>Soldiers</b></a></li>
        <li class="active"><a href="pillar.jsp" style="color: lightyellow"><b>Pillars</b></a></li>
        <li><a href="addSoldier.jsp" style="color: lightyellow"><b>Add a New Soldier</b></a></li>
        <li ><a href="addPillar.jsp" style="color: lightyellow"><b>Add a New Pillar</b></a></li>
        <li><a href="currentLocation.jsp?soldier=all&currentLoc=true" style="color: lightyellow"><b>Soldier's Current Location</b></a></li>
        <li><a href="customSearch.jsp" style="color: lightyellow"><b>Customized Search</b></a></li>
      </ul>
        
      <ul class="nav navbar-nav navbar-right">
         <%
         if(session.getAttribute("userId")!=null) {
         %>
        <form action="home.jsp" method="post">
            <li><span class="glyphicon glyphicon-user">Logged in as <%=session.getAttribute("username")%></span>
            <input name="logout" type="hidden" value="logout">
            <button type="submit" class="btn btn-danger">Logout</button>
        </li>
        </form>
        <%
        }else{
        %>
        <form action="home.jsp" method="post">
            <li></li>
            <li>Username<input name="username" type="text" required="" style="color: black"/><span class="glyphicon glyphicon-user"></span>
                Password<input name="password" type="password" required="" style="color: black"/><span class="glyphicon glyphicon-user"></span>
                <input type="submit" value="Login" style="color: black"/>
            </li>
        </form>
        <%}%>
      </ul>
    </div>
  </div>
</nav>
        <h3 style="margin-left: 100px">History of <%=request.getParameter("name")%>(<%=request.getParameter("id")%>)</h3>
<%
List<String> imageUrlList = new ArrayList<>(); 
int iter=0;
File imageDir = new File("C:\\Users\\Al-Amin\\Documents\\NetBeansProjects\\BGB\\web\\pillarImg");  
for(File imageFile : imageDir.listFiles()){  
  String imageFileName = imageFile.getName();  

  // add this images name to the list we are building up
if(request.getParameter("id")!=null && request.getParameter("id").equals(imageFileName.split("_")[0]))  {
  imageUrlList.add(imageFileName);  
}

}  

if(imageUrlList.size()>0){
Collections.sort(imageUrlList.subList(1,  imageUrlList.size()));
}
for( iter=0;iter<imageUrlList.size();iter++){  
String imgUrl = "pillarImg/"+imageUrlList.get(iter);
%>

<%
if((iter%3)==0) {
%>
<div class="container">    
  <div class="row">
<%}%>
    <div class="col-sm-4">
      <div class="panel panel-primary">
        <div class="panel-heading">Date Captured: <%=imageUrlList.get(iter).split("_")[3]%>/<%=imageUrlList.get(iter).split("_")[2]%>/<%=imageUrlList.get(iter).split("_")[1]%></div>
        <div class="panel-body"><img src='<%=imgUrl%>' class="img-responsive" style="width:100%; height: 300px" alt="Image"></div>
        <div class="panel-footer">Time Captured: <%=imageUrlList.get(iter).split("_")[4]%>:<%=imageUrlList.get(iter).split("_")[5]%>:<%=imageUrlList.get(iter).split("_")[6]%></div>
        <div class="panel-footer">Status: <%=imageUrlList.get(iter).split("_")[7]%></div>
      </div>
    </div>
    
 <%
if(((iter+1)%3)==0) {
%>
  </div>
</div><br>
<%}%>

<%
}
if((iter%3)!=0){
%>
</div>
</div><br>
<%}%>

<footer class="container-fluid text-center">
  <p>Footer</p>
</footer>

</body>
</html>

