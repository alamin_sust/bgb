<%-- 
    Document   : currentLocation
    Created on : Feb 25, 2017, 2:17:08 AM
    Author     : Al-Amin
--%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.sql.Date"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="com.bgb.connection.Database"%>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>BGB</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    .navbar {
      margin-bottom: 50px;
      border-radius: 0;
    }
    
    /* Remove the jumbotron's default bottom margin */ 
     .jumbotron {
      margin-bottom: 0;
    }
   
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
  </style>
        
      <style>
.dropbtn {
    background-color: #4CAF50;
    color: white;
    padding: 16px;
    font-size: 16px;
    border: none;
    cursor: pointer;
}

.dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

.dropdown-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}

.dropdown-content a:hover {background-color: #f1f1f1}

.dropdown:hover .dropdown-content {
    display: block;
}

.dropdown:hover .dropbtn {
    background-color: #3e8e41;
}
</style>
<script>
function SelectElement(valueToSelect)
{    
    var element = document.getElementById('soldier');
    element.value = valueToSelect;
    var element2 = document.getElementById('soldier2');
    element2.value = valueToSelect;
}
</script>
  <script src="http://maps.google.com/maps/api/js?sensor=false" 
          type="text/javascript"></script>
</head>
<body>

<%
    String formattedDate= new String();
    if(session.getAttribute("userId")==null){
                    response.sendRedirect("login.jsp");
                }
    
    Database db = new Database();
                db.connect();
                Statement st = db.connection.createStatement();
                String qry = "select * from soldier_location sl where sl.id>0";
                if(request.getParameter("soldier")!=null && !request.getParameter("soldier").equals("all"))
                {
                    qry+=" and sl.soldier_id="+request.getParameter("soldier");
                    
                }
                if(request.getParameter("date")!=null)
                {
                    qry+=" and sl.datetime like '"+request.getParameter("date")+"%'";
                }
                if(request.getParameter("currentLoc")!=null)
                {
                    
                    
                    
    
                   
                    qry+=" and sl.datetime >=(select MAX(datetime) from soldier_location sl2 where sl.soldier_id=sl2.soldier_id)";
                }
                
                qry+=" order by sl.soldier_id asc,sl.datetime desc";
                ResultSet rs = st.executeQuery(qry);
                
                int threeColor=0;
                if(request.getParameter("soldier")!=null && !request.getParameter("soldier").equals("all")
                        &&request.getParameter("currentLoc")==null)
                {
                    threeColor=1;
                }
    

ArrayList<String> names = new ArrayList<>();
ArrayList<String> locs = new ArrayList<>();
/*names.add("'Consteble Jubayer'");
locs.add(24.9156412);
locs.add(91.952099);
names.add("'Habildar Tanvir'");
locs.add(24.9161319);
locs.add(91.9533794);
names.add("'Night Guard Shapan'");
locs.add(24.9168021);
locs.add(91.9547929);
*/

ArrayList<Integer> currentLocation = new ArrayList<>();
while(rs.next()){
    Statement st2 = db.connection.createStatement();
                String qry2 = "select * from soldier where id="+rs.getString("soldier_id");
                
                java.util.Date date = new java.util.Date(System.currentTimeMillis()-60*1000);
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.s");
                    
                    formattedDate = sdf.format(date)+"0";
                    
                if(rs.getString("soldier_id")!=null){
                ResultSet rs2 = st2.executeQuery(qry2);
                
               
               rs2.next();
    if(formattedDate.compareTo(rs.getString("datetime"))<=0){
                currentLocation.add(1);
                        }
                    else{
                        currentLocation.add(0);
                    }
                
    locs.add("'"+rs.getString("soldier_id")+"'");
    locs.add("'"+rs2.getString("name")+"'");
    locs.add(rs.getString("latitude"));
    locs.add(rs.getString("longitude"));
    locs.add("'"+rs.getString("datetime")+"'");}
}



%>    
    
<div class="jumbotron" style="background-image: url(bg2.jpg); background-size: 100%;">
  <div class="container text-center">
    <img src="logo-bgb.jfif" class="img-circle" height="105" width="105" alt="Avatar"/>
    <b><h2 style="color: white">BGB Patrol Monitoring & Border Condition Reporting</h2></b>
  </div>
    <form action="pillar.jsp" method="get" class="navbar-form navbar-right" role="search">
        <div class="form-group input-group">
            <input type="text" name="findpillar" class="form-control" placeholder="Search Pillar" required="">
          <span class="input-group-btn">
            <button class="btn btn-default" type="submit">
              <span class="glyphicon glyphicon-search"></span>
            </button>
          </span>        
        </div>
      </form>
        <form action="soldier.jsp" method="get" class="navbar-form navbar-right" role="search">
        <div class="form-group input-group">
            <input type="text" name="findsoldier" class="form-control" placeholder="Search Soldier" required="">
          <span class="input-group-btn">
            <button class="btn btn-default" type="submit">
              <span class="glyphicon glyphicon-search"></span>
            </button>
          </span>        
        </div>
      </form>
</div>
   
    <nav class="navbar navbar-inverse" style="color: white; background: darkkhaki">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar"  >
      <ul class="nav navbar-nav" >
        <li><a href="home.jsp" style="color: lightyellow"><b>Home</b></a></li>
        <li><a href="soldier.jsp" style="color: lightyellow"><b>Soldiers</b></a></li>
        <li><a href="pillar.jsp" style="color: lightyellow"><b>Pillars</b></a></li>
        <li><a href="addSoldier.jsp" style="color: lightyellow"><b>Add a New Soldier</b></a></li>
        <li><a href="addPillar.jsp" style="color: lightyellow"><b>Add a New Pillar</b></a></li>
        <li class="active"><a href="currentLocation.jsp?soldier=all&currentLoc=true" style="color: lightyellow"><b>Soldier's Current Location</b></a></li>
        <li><a href="customSearch.jsp" style="color: lightyellow"><b>Customized Search</b></a></li>
      </ul>
        
        <br>
      <ul class="nav navbar-nav navbar-right">
         <%
         if(session.getAttribute("userId")!=null) {
         %>
        <form action="home.jsp" method="post">
            <li><span class="glyphicon glyphicon-user">Logged in as <%=session.getAttribute("username")%></span>
            <input name="logout" type="hidden" value="logout">
            <button type="submit" class="btn btn-danger">Logout</button>
        </li>
        </form>
        <%
        }else{
        %>
        <form action="home.jsp" method="post">
            <li></li>
            <li><input name="username" type="text" required="" placeholder="username" style="color: black"/><span class="glyphicon glyphicon-user"></span>
                <input name="password" type="password" required="" placeholder="password" style="color: black"/><span class="glyphicon glyphicon-user"></span>
                <input type="submit" value="insert" style="color: black"/>
            </li>
        </form>
        <%}%>
      </ul>
    </div>
  </div>
</nav>
  <%
      if(request.getParameter("name")!=null){
          %>
<div class="container-fluid text-center">  
     <div class="alert alert-success fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">�</a>
        <p><strong>Success!</strong></p>
        Pillar Inserted Into Central Database Successfully.
      </div>
    <%
        request.setAttribute("name", null);
        
    }%>
  <div class="row content">
      <h3 style="margin-left: 100px; color: darkgreen"><b>Current and Last Locations of Patrolling Soldiers</b></h3>
      
      
       
   

<div style="margin-left: 300px" class="dropdown">
   <h3> <b>Customized Search For All Locations: </b></h3>
   <form action="currentLocation.jsp" method="get">
    <select id="soldier" name="soldier">
        <option value="all">All</option>
        <%
        Statement st3 = db.connection.createStatement();
                String qry3 = "select * from soldier";
                
                
                
                ResultSet rs3 = st3.executeQuery(qry3);
                while(rs3.next()){
        %>
        <option value="<%=rs3.getString("id")%>"><%=rs3.getString("id")%> : <%=rs3.getString("name")%></option>
        <%
        }
        %>
    </select>
    
    <input type="date" name="date" placeholder="Date">
    <button class="dropbtn" type="submit">Search</button>
   </form>
    
    
    <h3> <b>Customized Search For Current and Last Location: </b></h3>
   <form action="currentLocation.jsp" method="get">
    <select id="soldier2" name="soldier">
        <option value="all">All</option>
        <%
        Statement st4 = db.connection.createStatement();
                String qry4 = "select * from soldier";
                
                
                
                ResultSet rs4 = st4.executeQuery(qry4);
                while(rs4.next()){
        %>
        <option value="<%=rs4.getString("id")%>"><%=rs4.getString("id")%> : <%=rs4.getString("name")%></option>
        <%
        }
        %>
    </select>
    <input type="hidden" name="currentLoc" value="true">
    <button class="dropbtn" type="submit">Search</button>
   </form>
</div>
 
        <%if(request.getParameter("currentLoc")!=null){%>
     <h3 style="margin-left: 100px; color: darkred"><b>Current and Last Location Search Results</b></h3>
     <%}else{%>
     <h3 style="margin-left: 100px; color: darkblue"><b>Location History</b></h3>
     <%}%>
      <div id="map" style="width: 1200px; height: 600px; margin-left: 100px"></div>
   <%if(request.getParameter("soldier") != null && !request.getParameter("soldier").equals("all")) {  
  %><script>SelectElement(<%=request.getParameter("soldier")%>);</script><%   }%>
      
  
  <%
      int lastPoint=0;
  java.util.Date date2 = new java.util.Date(System.currentTimeMillis()-60*1000);
                    SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.s");
                    
                    String formattedDate2 = sdf2.format(date2)+"0";
                    if(locs.size()>=5&&formattedDate2.compareTo(locs.get(locs.size()-1).substring(1,locs.get(locs.size()-1).length()-1))<=0){
                        lastPoint=1;
                    }
  
  %>
  
  <script type="text/javascript">
    var locations = <%=locs%>;
    var threeColor= <%=threeColor%>;
    var lastPoint= <%=lastPoint%>;
    var currentLocation = <%=currentLocation%>;
    
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 9,
      center: new google.maps.LatLng(24.9118006, 91.8344068),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    var mk = ['blue-dot','red-dot','yellow-dot','green-dot'];

    


    for (i = 0+(threeColor*5); i < locations.length-(threeColor*5);i+=5) {  
      var colorIndex = locations[i]%4;
      
      if(threeColor==1 && i==0) {
          colorIndex=1;
      }
      else if(threeColor==1 && (i+5)==locations.length) {
          colorIndex=3;
      }
      else if(threeColor==1)
      {
          colorIndex=0;
      }
      
      
      if((i/5)<currentLocation.length) {
          if(currentLocation[i/5]==1){
              colorIndex=3;
          }
          else {
              colorIndex=1;
          }
      }
      
      marker = new google.maps.Marker({
       
            icon: 'http://maps.google.com/mapfiles/ms/icons/'+mk[colorIndex]+'.png',
        position: new google.maps.LatLng(locations[i+2], locations[i+3]),
        map: map
      });
      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
        if((i/5)<currentLocation.length && currentLocation[i/5]==1) {
            infowindow.setContent("id: "+locations[i]+"<br>name: "+locations[i+1]+"<br>time: "+locations[i+4]+"<br>Current Status: Active");
        }
        else if((i/5)<currentLocation.length && currentLocation[i/5]==0) {
            infowindow.setContent("id: "+locations[i]+"<br>name: "+locations[i+1]+"<br>time: "+locations[i+4]+"<br>Current Status: Inactive");
        }
        else {
          infowindow.setContent("id: "+locations[i]+"<br>name: "+locations[i+1]+"<br>time: "+locations[i+4]);
      }
      infowindow.open(map, marker);
          
        }
      })(marker, i));
      
      if(threeColor==1){
      var line = new google.maps.Polyline({
    path: [
        new google.maps.LatLng(locations[i+2], locations[i+3]), 
        new google.maps.LatLng(locations[i+7], locations[i+8])
    ],
    strokeColor: "#FF0000",
    strokeOpacity: 1.0,
    strokeWeight: 2,
    map: map
      }
);
      
    }
    
     
    }
    
    if(threeColor==1) {
        if(locations.length>=10) {
        var line = new google.maps.Polyline({
    path: [
        new google.maps.LatLng(locations[2], locations[3]), 
        new google.maps.LatLng(locations[7], locations[8])
    ],
    strokeColor: "#FF0000",
    strokeOpacity: 1.0,
    strokeWeight: 2,
    map: map
      }
);
        }
    }
    
    if(threeColor==1)
    {
        
        
        i=locations.length-5;
        colorIndex=3;
        var image= 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
      
        marker = new google.maps.Marker({
       
            icon: image,
        position: new google.maps.LatLng(locations[i+2], locations[i+3]),
        map: map,
        label: "start"
      
      });
      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
        
          infowindow.setContent("id: "+locations[i]+"<br>name: "+locations[i+1]+"<br>time: "+locations[i+4]);
          infowindow.open(map, marker);
          
        }
      })(marker, i));
      var label;
      if(lastPoint==1){
            image= 'http://maps.google.com/mapfiles/ms/icons/'+mk[3]+'.png';
            label="patrolling"
        }
        else{
            image= 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
                label="end"
    }
      i=0;
        colorIndex=1;
        
        marker = new google.maps.Marker({
       
            icon: image,
        position: new google.maps.LatLng(locations[i+2], locations[i+3]),
        map: map,
        label: label
      });
      
      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
        
          infowindow.setContent("id: "+locations[i]+"<br>name: "+locations[i+1]+"<br>time: "+locations[i+4]);
          infowindow.open(map, marker);
          
        }
      })(marker, i));
    }
  </script>
      
      
  </div>
</div>

<footer class="container-fluid text-center">
  <p>Footer Text</p>
</footer>

</body>
</html>

