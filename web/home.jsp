<%-- 
    Document   : home
    Created on : Feb 7, 2017, 12:19:15 AM
    Author     : Al-Amin
--%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="com.bgb.connection.Database" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>BGB Patrol Monitoring & Border Condition Reporting</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    .navbar {
      margin-bottom: 50px;
      border-radius: 0;
    }
    
    /* Remove the jumbotron's default bottom margin */ 
     .jumbotron {
      margin-bottom: 0;
    }
   
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
  </style>
</head>
<body>

    <%
        if(request.getParameter("logout")!=null) {
                    session.setAttribute("userId", null);
                }
        
        Database db = new Database();
                db.connect();
                Statement stmt = db.connection.createStatement();
                
                
                
                String username = request.getParameter("username");
                String password = request.getParameter("password");
                if(username != null && password != null) {
                    String query = "select id,password from admin where username like '"+username+"'";
                    ResultSet rs = stmt.executeQuery(query);
                    while(rs.next()==true && request.getParameter("password").equals(rs.getString("password")) ) {
                         session.setAttribute("userId", rs.getString("id") );
                         session.setAttribute("username", username );
                    }
                }
        
        if(session.getAttribute("userId")==null){
                    response.sendRedirect("login.jsp");
                }
        
             
    %>
    
    <div class="jumbotron" style="background-image: url(bg2.jpg); background-size: 100%;">
  <div class="container text-center">
    <img src="logo-bgb.jfif" class="img-circle" height="105" width="105" alt="Avatar"/>
    <b><h2 style="color: white">BGB Patrol Monitoring & Border Condition Reporting</h2></b>
  </div>
    <form action="pillar.jsp" method="get" class="navbar-form navbar-right" role="search">
        <div class="form-group input-group">
            <input type="text" name="findpillar" class="form-control" placeholder="Search Pillar" required="">
          <span class="input-group-btn">
            <button class="btn btn-default" type="submit">
              <span class="glyphicon glyphicon-search"></span>
            </button>
          </span>        
        </div>
      </form>
        <form action="soldier.jsp" method="get" class="navbar-form navbar-right" role="search">
        <div class="form-group input-group">
            <input type="text" name="findsoldier" class="form-control" placeholder="Search Soldier" required="">
          <span class="input-group-btn">
            <button class="btn btn-default" type="submit">
              <span class="glyphicon glyphicon-search"></span>
            </button>
          </span>        
        </div>
      </form>
</div>
   
    <nav class="navbar navbar-inverse" style="color: white; background: darkkhaki">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar"  >
      <ul class="nav navbar-nav" >
          <li class="active"><a href="home.jsp" style="color: lightyellow"><b>Home</b></a></li>
        <li><a href="soldier.jsp" style="color: lightyellow"><b>Soldiers</b></a></li>
        <li><a href="pillar.jsp" style="color: lightyellow"><b>Pillars</b></a></li>
        <li><a href="addSoldier.jsp" style="color: lightyellow"><b>Add a New Soldier</b></a></li>
        <li><a href="addPillar.jsp" style="color: lightyellow"><b>Add a New Pillar</b></a></li>
        <li><a href="currentLocation.jsp?soldier=all&currentLoc=true" style="color: lightyellow"><b>Soldier's Current Location</b></a></li>
        <li><a href="customSearch.jsp" style="color: lightyellow"><b>Customized Search</b></a></li>
      </ul>
        <br>
      <ul class="nav navbar-nav navbar-right">
         <%
         if(session.getAttribute("userId")!=null) {
         %>
        <form action="home.jsp" method="post">
            <li><span class="glyphicon glyphicon-user">Logged in as <%=session.getAttribute("username")%></span>
            <input name="logout" type="hidden" value="logout">
            <button type="submit" class="btn btn-danger">Logout</button>
        </li>
        </form>
        <%
        }else{
        %>
        <form action="home.jsp" method="post">
            <li></li>
            <li><input name="username" type="text" required="" placeholder="username" style="color: black"/><span class="glyphicon glyphicon-user"></span>
                <input name="password" type="password" required="" placeholder="password" style="color: black"/><span class="glyphicon glyphicon-user"></span>
                <input type="submit" value="Login" style="color: black"/>
            </li>
        </form>
        <%}%>
      </ul>
    </div>
  </div>
</nav>
  
<div class="container text-center">    
  <div class="row">
    <div class="col-sm-3 well">
      <div class="well">
        <img src="admin.png" class="img-circle" height="105" width="105" alt="Avatar">
        <p><a href="#">My Profile</a></p>
      </div>
      
      <p><a href="#">Link</a></p>
      <p><a href="#">Link</a></p>
      <p><a href="#">Link</a></p>
    </div>
    <div class="col-sm-7">
    
      <div class="row">
        <div class="col-sm-12">
          <div class="panel panel-default text-left">
            <div class="panel-body">
                 
               <form type="post" action="home.jsp">
                   <input type="text" name="broadcast" class="form-control" placeholder="Urgent Announcement" required>
              <button type="submit" class="btn btn-danger">
                <span></span> Post
              </button>
               </form>
               
            </div>
          </div>
        </div>
      </div>
         <h2>Recent Captures</h2> 
      <%
      String qryRecentActivities = "select * from capture order by(time_captured) desc";
      Statement stRecentActivities =  db.connection.createStatement();
      ResultSet rsRecentActivities = stRecentActivities.executeQuery(qryRecentActivities);
      int listSize = 5;
      
      for(int i=0;i<5&&rsRecentActivities.next();i++) {
          String dt[] = rsRecentActivities.getString("time_captured").split("-");
          
          String qrySoldier = "select * from soldier where id="+rsRecentActivities.getString("soldier_id");
          Statement stSoldier =  db.connection.createStatement();
          ResultSet rsSoldier = stSoldier.executeQuery(qrySoldier);
          rsSoldier.next();
          
          String qryPillar = "select * from pillar where id="+rsRecentActivities.getString("pillar_id");
          Statement stPillar =  db.connection.createStatement();
          ResultSet rsPillar = stPillar.executeQuery(qryPillar);
          rsPillar.next();
          

      %>
      <div class="row">
        <div class="col-sm-3">
          <div class="well">
           <p>Captured By:</p>
           <p><%=rsSoldier.getString("name")%></p>
           <p><%=rsSoldier.getString("designation")%></p>
           <p><%=dt[2]+"/"+dt[1]+"/"+dt[0]+" "+dt[3]+":"+dt[4]%></p>
           <img src="soldier.jpg" class="img-circle" height="55" width="55" alt="Avatar">
          </div>
        </div>
        <div class="col-sm-9">
          <div class="well">
              <h4><%=rsPillar.getString("name")%></h4>
              <h4>Pillar Number: <%=rsPillar.getString("number")%></h4>
              <h4>Current Status: <%=rsPillar.getString("situation")%></h4>
            <p><img src="pillar.jpg" height="100" width="150" alt="Avatar"></p>
          </div>
        </div>
      </div>
      <%
      }
      %>     
      </div>
      
      
    <div class="col-sm-2 well">
      <div class="thumbnail">
        <p>Upcoming Events:</p>
        <img src="soldier.jpg" alt="Paris" width="400" height="300">
        <p><strong>Sylhet</strong></p>
        <p>Friday, 17 February 2017</p>
        <button class="btn btn-primary">Info</button>
      </div>      
      <div class="well">
        <p>ADS</p>
      </div>
      <div class="well">
        <p>ADS</p>
      </div>
    </div>
  </div>
</div>

<footer class="container-fluid text-center">
  <p>Footer Text</p>
</footer>
<%

%>
</body>
</html>

