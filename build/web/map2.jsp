<%-- 
    Document   : map2
    Created on : Feb 25, 2017, 12:14:38 AM
    Author     : Al-Amin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<script type="text/javascript"src="http://www.google.com/jsapi?key=AIzaSyB6ClKuAtPeyDLYOrrIW-Jdt6wWZVFGSO0">
<script type="text/javascript"charset="utf-8">google.load("maps","2.x"); google.load("jquery","1.3.1");</script>


<div id="map"></div>
<style>#map { width:500px; height:500px; }</style>

$(document).ready(function(){ 
  var map = new GMap2(document.getElementById('map')); 
  var burnsvilleMN = new GLatLng(44.797916,-93.278046); 
  map.setCenter(burnsvilleMN, 8); 
});




// setup 10 random points 
var bounds = map.getBounds(); 
var southWest = bounds.getSouthWest(); 
var northEast = bounds.getNorthEast(); 
var lngSpan = northEast.lng() - southWest.lng(); 
var latSpan = northEast.lat() - southWest.lat(); 
var markers = []; 
for (var i = 0; i<10; i++) { 
  var point = new GLatLng(southWest.lat() + latSpan * Math.random(), southWest.lng() + lngSpan * Math.random()); 
  marker = new GMarker(point); 
  map.addOverlay(marker); 
  markers[i] = marker; 
}
[/code]</p>
 
<p>Note that I added a markers array to the example code. This will be used in the next step.</p>
 
<h2>Step #5: Loop Through Markers and Add Basic Click Event to Markers</h2>
 
<p>In this step, we start to use jQuery and Google Maps together. We want to be careful to use Google Map’s built-in <span class="caps">API</span> as much as possible, leaving jQuery only for what it is best at.</p>
 
<p><object width="462" height="264">
<param value="http://marcgrabanski.com/jing/google-maps-tutorial-pan.swf" name="movie" />
<param value="true" name="allowFullScreen" />
<param value="always" name="allowscriptaccess" /><embed width="462" height="264" allowfullscreen="true" allowscriptaccess="always" type="application/x-shockwave-flash" src="http://marcgrabanski.com/jing/google-maps-tutorial-pan.swf"></embed></object></p>
 
<p>Let’s take that array of markers and loop through them with <a href="http://docs.jquery.com/Utilities/jQuery.each">jQuery’s each method</a>.</p>
 
<p>[code lang="js"]
$(markers).each(function(i,marker){ 
  GEvent.addListener(marker,"click", function(){ 
    map.panTo(marker.getLatLng()); 
  }); 
});