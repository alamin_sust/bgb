<%-- 
    Document   : admin
    Created on : Feb 4, 2017, 5:48:07 PM
    Author     : Al-Amin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <%
        session.setAttribute("login", "abcd");
        request.setAttribute("pass", "1234");
        %>
        <%=request.getAttribute("pass")%>
    </body>
</html>
