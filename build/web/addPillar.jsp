<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="com.bgb.connection.Database"%>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>BGB</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    .navbar {
      margin-bottom: 50px;
      border-radius: 0;
    }
    
    /* Remove the jumbotron's default bottom margin */ 
     .jumbotron {
      margin-bottom: 0;
    }
   
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
  </style>
</head>
<body>

<%
    if(session.getAttribute("userId")==null){
                    response.sendRedirect("login.jsp");
                }
    
if(request.getParameter("name")!=null) {
    
    Database db = new Database();
                db.connect();
                Statement st = db.connection.createStatement();
                String mxIdQry = "select max(id)+1 as mxid from pillar";
                ResultSet rs = st.executeQuery(mxIdQry);
                String number  = "1";
                String longitude  = "null";
                String latitude  = "null";
                rs.next();
                String id = rs.getString("mxid");
                String name=request.getParameter("name").toString();
                if(request.getParameter("number").toString()!=null&&request.getParameter("number").toString().length()>0){
                number  = request.getParameter("number").toString();
                }
                if(request.getParameter("longitude").toString()!=null&& request.getParameter("longitude").toString().length()>0 ){
                longitude  = request.getParameter("longitude").toString();
                }
                if(request.getParameter("latitude").toString()!=null&&request.getParameter("latitude").toString().length()>0){
                latitude  = request.getParameter("latitude").toString();
                }
                String query = "insert into pillar(id,name,number,longitude,latitude) values("+id+",'"+name+"',"+number+", '"+longitude+"', '"+latitude+"')";
                Statement stIns = db.connection.createStatement();
                stIns.executeUpdate(query);
}


%>    
    
    
<div class="jumbotron" style="background-image: url(bg2.jpg); background-size: 100%;">
  <div class="container text-center">
    <img src="logo-bgb.jfif" class="img-circle" height="105" width="105" alt="Avatar"/>
    <b><h2 style="color: white">BGB Patrol Monitoring & Border Condition Reporting</h2></b>
  </div>
    <form action="pillar.jsp" method="get" class="navbar-form navbar-right" role="search">
        <div class="form-group input-group">
            <input type="text" name="findpillar" class="form-control" placeholder="Search Pillar" required="">
          <span class="input-group-btn">
            <button class="btn btn-default" type="submit">
              <span class="glyphicon glyphicon-search"></span>
            </button>
          </span>        
        </div>
      </form>
        <form action="soldier.jsp" method="get" class="navbar-form navbar-right" role="search">
        <div class="form-group input-group">
            <input type="text" name="findsoldier" class="form-control" placeholder="Search Soldier" required="">
          <span class="input-group-btn">
            <button class="btn btn-default" type="submit">
              <span class="glyphicon glyphicon-search"></span>
            </button>
          </span>        
        </div>
      </form>
</div>
   
    <nav class="navbar navbar-inverse" style="color: white; background: darkkhaki">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar"  >
      <ul class="nav navbar-nav" >
        <li><a href="home.jsp" style="color: lightyellow"><b>Home</b></a></li>
        <li><a href="soldier.jsp" style="color: lightyellow"><b>Soldiers</b></a></li>
        <li><a href="pillar.jsp" style="color: lightyellow"><b>Pillars</b></a></li>
        <li><a href="addSoldier.jsp" style="color: lightyellow"><b>Add a New Soldier</b></a></li>
        <li class="active"><a href="addPillar.jsp" style="color: lightyellow"><b>Add a New Pillar</b></a></li>
        <li><a href="currentLocation.jsp?soldier=all&currentLoc=true" style="color: lightyellow"><b>Soldier's Current Location</b></a></li>
        <li><a href="customSearch.jsp" style="color: lightyellow"><b>Customized Search</b></a></li>
      </ul>
        <br>
      <ul class="nav navbar-nav navbar-right">
         <%
         if(session.getAttribute("userId")!=null) {
         %>
        <form action="home.jsp" method="post">
            <li><span class="glyphicon glyphicon-user">Logged in as <%=session.getAttribute("username")%></span>
            <input name="logout" type="hidden" value="logout">
            <button type="submit" class="btn btn-danger">Logout</button>
        </li>
        </form>
        <%
        }else{
        %>
        <form action="home.jsp" method="post">
            <li></li>
            <li><input name="username" type="text" required="" placeholder="username" style="color: black"/><span class="glyphicon glyphicon-user"></span>
                <input name="password" type="password" required="" placeholder="password" style="color: black"/><span class="glyphicon glyphicon-user"></span>
                <input type="submit" value="insert" style="color: black"/>
            </li>
        </form>
        <%}%>
      </ul>
    </div>
  </div>
</nav>
  <%
      if(request.getParameter("name")!=null){
          %>
<div class="container-fluid text-center">  
     <div class="alert alert-success fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">�</a>
        <p><strong>Success!</strong></p>
        Pillar Inserted Into Central Database Successfully.
      </div>
    <%
        request.setAttribute("name", null);
        
    }%>
  <div class="row content">
    <div class="col-sm-2 sidenav">
    </div>
    <div class="col-sm-8 text-left"> 
      <h1>Add a New Pillar</h1>
      <br>
      <form class="form-inline" method="post" action="addPillar.jsp">
      Name:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <input type="text" name="name" class="form-control" size="50" placeholder="Name" required>
      <br><br>
      Number:&nbsp;&nbsp;&nbsp;&nbsp; <input type="number" name="number" class="form-control" size="50" placeholder="Number" >
      <br><br>
      Longitude: <input type="text" name="longitude" class="form-control" size="50" placeholder="Longitude" >
      <br><br>
      Latitude: &nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="latitude" class="form-control" size="50" placeholder="Latitude">
      <br><br>
      <button type="submit" class="btn btn-danger">Insert</button>
      </form>
    </div>
    <div class="col-sm-2 sidenav">
      
    </div>
  </div>
</div>

<footer class="container-fluid text-center">
  <p>Footer Text</p>
</footer>

</body>
</html>
