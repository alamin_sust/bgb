<%-- 
    Document   : soldier
    Created on : Feb 9, 2017, 1:02:14 AM
    Author     : Al-Amin
--%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="com.bgb.connection.Database"%>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
    /* Remove the navbar's default rounded borders and increase the bottom margin */ 
    .navbar {
      margin-bottom: 50px;
      border-radius: 0;
    }
    
    /* Remove the jumbotron's default bottom margin */ 
     .jumbotron {
      margin-bottom: 0;
    }
   
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
  </style>
</head>
<body>
<%
    if(session.getAttribute("userId")==null){
                    response.sendRedirect("login.jsp");
                }
    
             Database db = new Database();
                db.connect();
                Statement stmt = db.connection.createStatement();
                
                if(session.getAttribute("userId")==null){
                    response.sendRedirect("home.jsp");
                }
                
                if(request.getParameter("logout")!=null) {
                    session.setAttribute("userId", null);
                }
                
                String username = request.getParameter("username");
                String password = request.getParameter("password");
                if(username != null && password != null) {
                    String query = "select id,password from admin where username like '"+username+"'";
                    ResultSet rs = stmt.executeQuery(query);
                    while(rs.next()==true && request.getParameter("password").equals(rs.getString("password")) ) {
                         session.setAttribute("userId", rs.getString("id") );
                         session.setAttribute("username", username );
                    }
                }
    %>
<div class="jumbotron" style="background-image: url(bg2.jpg); background-size: 100%;">
  <div class="container text-center">
    <img src="logo-bgb.jfif" class="img-circle" height="105" width="105" alt="Avatar"/>
    <b><h2 style="color: white">BGB Patrol Monitoring & Border Condition Reporting</h2></b>
  </div>
    <form action="pillar.jsp" method="get" class="navbar-form navbar-right" role="search">
        <div class="form-group input-group">
            <input type="text" name="findpillar" class="form-control" placeholder="Search Pillar" required="">
          <span class="input-group-btn">
            <button class="btn btn-default" type="submit">
              <span class="glyphicon glyphicon-search"></span>
            </button>
          </span>        
        </div>
      </form>
        <form action="soldier.jsp" method="get" class="navbar-form navbar-right" role="search">
        <div class="form-group input-group">
            <input type="text" name="findsoldier" class="form-control" placeholder="Search Soldier" required="">
          <span class="input-group-btn">
            <button class="btn btn-default" type="submit">
              <span class="glyphicon glyphicon-search"></span>
            </button>
          </span>        
        </div>
      </form>
</div>
   
    <nav class="navbar navbar-inverse" style="color: white; background: darkkhaki">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar"  >
      <ul class="nav navbar-nav" >
        <li><a href="home.jsp" style="color: lightyellow"><b>Home</b></a></li>
        <li class="active"><a href="soldier.jsp" style="color: lightyellow"><b>Soldiers</b></a></li>
        <li><a href="pillar.jsp" style="color: lightyellow"><b>Pillars</b></a></li>
        <li><a href="addSoldier.jsp" style="color: lightyellow"><b>Add a New Soldier</b></a></li>
        <li><a href="addPillar.jsp" style="color: lightyellow"><b>Add a New Pillar</b></a></li>
        <li><a href="currentLocation.jsp?soldier=all&currentLoc=true" style="color: lightyellow"><b>Soldier's Current Location</b></a></li>
        <li><a href="customSearch.jsp" style="color: lightyellow"><b>Customized Search</b></a></li>
      </ul>
        <br>
      <ul class="nav navbar-nav navbar-right">
         <%
         if(session.getAttribute("userId")!=null) {
         %>
        <form action="home.jsp" method="post">
            <li><span class="glyphicon glyphicon-user">Logged in as <%=session.getAttribute("username")%></span>
            <input name="logout" type="hidden" value="logout">
            <button type="submit" class="btn btn-danger">Logout</button>
        </li>
        </form>
        <%
        }else{
        %>
        <form action="home.jsp" method="post">
            <li></li>
            <li>Username<input name="username" type="text" required="" style="color: black"/><span class="glyphicon glyphicon-user"></span>
                Password<input name="password" type="password" required="" style="color: black"/><span class="glyphicon glyphicon-user"></span>
                <input type="submit" value="Login" style="color: black"/>
            </li>
        </form>
        <%}%>
      </ul>
    </div>
  </div>
</nav>

<%
int iter=0;
String query = "select * from soldier where id>19";
if(request.getParameter("findsoldier")!=null)
{     
          String search = request.getParameter("findsoldier").toString();
          query+=" and (name like '"+search+"' or designation like '"+search+"')";
}

Statement st = db.connection.createStatement();
ResultSet rs  = st.executeQuery(query);

for(iter=0;rs.next();iter++){        
%>

<%
if((iter%3)==0) {
%>
<div class="container">    
  <div class="row">
<%}%>
    <div class="col-sm-4">
      <div class="panel panel-primary">
        <div class="panel-heading"><%=rs.getString("name")%></div>
        <div class="panel-body"><img src="soldier.jpg" class="img-responsive" style="width:100%" alt="Image"></div>
        <div class="panel-footer">Designation: <%=rs.getString("designation")%></div>
      </div>
    </div>
    
 <%
if(((iter+1)%3)==0) {
%>
  </div>
</div><br>
<%}%>

<%
}
if((iter%3)!=0){
%>
</div>
</div><br>
<%}%>

<footer class="container-fluid text-center">
  <p>Footer</p>
</footer>

</body>
</html>

